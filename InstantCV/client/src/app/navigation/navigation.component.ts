import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  public loggedIn:boolean = false;

  constructor(private authService: AuthService) {
    let item = this.authService.getWithExpiry('auth-token');
    if(item == null)
      this.loggedIn = false;
    else
      this.loggedIn = true;  
   }

   logout() {
    this.authService.logout();
    this.loggedIn = false;
  }

  ngOnInit(): void {
  }
}
