import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

@Component({
  selector:    'app-password',
  templateUrl: './password.component.html',
  styleUrls:   ['./password.component.css']
})
export class PasswordComponent implements OnInit {

  public emailForm: FormGroup;
  public passwordForm: FormGroup;
  public validUser: boolean;

  public hide:boolean = true;

  private email: string = '';

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private http: HttpClient) {
    this.emailForm = this.formBuilder.group({
      email:    ['', [Validators.required, Validators.email]],
    });

    this.passwordForm = this.formBuilder.group({
      code:     ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });

    this.validUser = true;
  }

  ngOnInit(): void {}

  public submitPass(data) {
    data['email'] = this.email;
    console.log(data);

    // Sending data to server
    this.http.post<any>(environment.serverUrl + '/user/reset/token', data).subscribe(someData => {
        this.router.navigateByUrl('/');
    },
    err => {
    });

    return;
  }

  public submitEmail(data) {
    console.log(data);

    if (!this.emailForm.valid) {
      window.alert('Not valid!');
      return;
    }
    // Sending data to server
    this.http.post<any>(environment.serverUrl + '/user/reset', data).subscribe(someData => {
        document.getElementById('container').classList.add('right-panel-active');
        this.email = data.email;
    },
    err => {
    });

    return;
  }

}
